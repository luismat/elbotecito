package com.elbotecito.database;

import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.model.Hijo;
import com.elbotecito.util.ArbolAVL;


import java.util.List;

public interface HijoDAO {
    //CRUD de Hijo - Crear, Leer, Actualizar y Eliminar.

    //Create
    public void guardarHijo(Hijo hijo) throws LlaveDuplicadaException;

    //Read
    public Hijo consultarHijoPorIdentificacion(String idHijo);

    public List<Hijo> consultarHijos();

    //Update
    public boolean actualizarHijo(Hijo hijo);

    //Delete
    public boolean eliminarHijo(String idHijo);
    ArbolAVL getArbolAVL();
}

package com.elbotecito.database;

import com.elbotecito.database.exception.LlaveDuplicadaException;

import java.util.List;

import com.elbotecito.model.Barco;
import com.elbotecito.util.ArbolAVL;

public interface BarcoDAO {
    //CRUD de Barco - Crear, Leer, Actualizar y Eliminar.

    //Create

    /**
     *
     * @param barco
     * @return
     * @throws LlaveDuplicadaException
     */
    void guardarBarco(Barco barco) throws LlaveDuplicadaException;

    //Read

    /**
     *
     * @param matriculaBarco
     * @return
     */
    Barco consultarBarcoPorMatricula(String matriculaBarco);

    List<Barco> consultarBarcos();

    //Update
    boolean actualizarBarco(Barco barco);

    //Delete
    boolean eliminarBarco(String matriculaBarco);

    ArbolAVL getArbolAVL();
}

package com.elbotecito.database;

import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.model.Esposa;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public interface EsposaDAO {
    //CRUD de Esposa - Crear, Leer, Actualizar y Eliminar.

    //Create
    public void guardarEsposa(Esposa esposa) throws LlaveDuplicadaException;

    //Read
    public Esposa consultarEsposaPorIdentificacion(String idEsposa);

    public List<Esposa> consultarEsposas();

    //Update
    public boolean actualizarEsposa(Esposa esposa);

    //Delete
    public boolean eliminarEsposa(String idEsposa);
    ArbolAVL getArbolAVL();
}

package com.elbotecito.database;

import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.model.Capitan;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public interface CapitanDAO {
    //CRUD de Capitan - Crear, Leer, Actualizar y Eliminar.

    //Create
    public void guardarCapitan(Capitan capitan) throws LlaveDuplicadaException;

    //Read
    public Capitan consultarCapitanPorIdentificacion(String idCapitan);

    public List<Capitan> consultarCapitanes();

    //Update
    public boolean actualizarCapitan(Capitan capitan);

    //Delete
    public boolean eliminarCapitan(String idCapitan);

    ArbolAVL getArbolAVL();
}

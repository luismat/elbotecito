package com.elbotecito.database;

import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.model.Puerto;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public interface PuertoDAO {
    //CRUD de Puerto - Crear, Leer, Actualizar y Eliminar.

    //Create
    public void guardarPuerto(Puerto puerto) throws LlaveDuplicadaException;

    //Read
    public Puerto consultarPuertoPorIdentificacion(String idPuerto);

    public List<Puerto> consultarPuertos();

    //Update
    public boolean actualizarPuerto(Puerto puerto);

    //Delete
    public boolean eliminarPuerto(String idPuerto);
    ArbolAVL getArbolAVL();
}

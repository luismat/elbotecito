package com.elbotecito.rules;

import com.elbotecito.rules.exception.EscalaYaExisteException;
import com.elbotecito.database.EscalaDAO;
import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.database.implementation.EscalaDAONIO;
import com.elbotecito.model.Escala;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public class EscalaBsn {

    private EscalaDAO escalaDAO;

    public EscalaBsn() {
        this.escalaDAO = new EscalaDAONIO();
    }

    public void guardarEscala(Escala escala) throws EscalaYaExisteException {
        try {
            this.escalaDAO.guardarEscala(escala);
        } catch (LlaveDuplicadaException lde) {
            throw new EscalaYaExisteException();
        }
    }

    public Escala consultarEscala(String idRuta, String idPuerto) {
        return this.escalaDAO.consultarEscala(idRuta, idPuerto);
    }

    public List<Escala> consultarEscalasPorIdRuta(String idRuta) {
        List<Escala> listaEscalas = this.escalaDAO.consultarEscalaPorIdRuta(idRuta);
        return listaEscalas;

    }

    public List<Escala> consultarEscalas() {
        List<Escala> listaEscalas = this.escalaDAO.consultarEscalas();
        return listaEscalas;

    }

    public void actualizarEscala(Escala escala) {
        this.escalaDAO.actualizarEscala(escala);
    }

    public void eliminarEscala(String idRuta, String idPuerto) {
        this.escalaDAO.eliminarEscala(idRuta, idPuerto);

    }
    public ArbolAVL getArbol() {
        return escalaDAO.getArbolAVL();
    }

}

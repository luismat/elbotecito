package com.elbotecito.rules;

import com.elbotecito.rules.exception.BarcoYaExisteException;
import com.elbotecito.database.*;
import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.database.implementation.BarcoDAONIO;
import com.elbotecito.model.Barco;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public class BarcoBsn {


    private BarcoDAO barcoDAO;

    public BarcoBsn() {
        this.barcoDAO = new BarcoDAONIO();
    }

    public void guardarBarco(Barco barco) throws BarcoYaExisteException {
        try {
            this.barcoDAO.guardarBarco(barco);
        } catch (LlaveDuplicadaException lde) {
            throw new BarcoYaExisteException();
        }
    }

    public Barco consultarBarco(String matricula) {
        return this.barcoDAO.consultarBarcoPorMatricula(matricula);
    }


    public List<Barco> consultarBarcos() {
        List<Barco> listaBarcos = this.barcoDAO.consultarBarcos();
        return listaBarcos;

    }

    public boolean actualizarBarco(Barco barco) {
        return this.barcoDAO.actualizarBarco(barco);
    }

    public boolean eliminarBarco(String matricula) {
        return this.barcoDAO.eliminarBarco(matricula);

    }

    public ArbolAVL getArbol() {
        return barcoDAO.getArbolAVL();
    }
}

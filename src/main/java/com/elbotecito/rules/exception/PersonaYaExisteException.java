package com.elbotecito.rules.exception;

public class PersonaYaExisteException extends  Exception{
    @Override
    public String getMessage() {
        return "La persona ya se encontraba almacenado previamente";
    }
}

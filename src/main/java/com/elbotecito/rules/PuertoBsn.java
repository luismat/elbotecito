package com.elbotecito.rules;

import com.elbotecito.rules.exception.PuertoYaExisteException;
import com.elbotecito.database.PuertoDAO;
import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.database.implementation.PuertoDAONIO;
import com.elbotecito.model.Puerto;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public class PuertoBsn {

    private PuertoDAO puertoDAO;

    public PuertoBsn() {
        this.puertoDAO = new PuertoDAONIO();
    }

    public void guardarPuerto(Puerto puerto) throws PuertoYaExisteException {
        try {
            this.puertoDAO.guardarPuerto(puerto);
        } catch (LlaveDuplicadaException lde) {
            throw new PuertoYaExisteException();
        }
    }

    public Puerto consultarPuerto(String identificacion) {
        return this.puertoDAO.consultarPuertoPorIdentificacion(identificacion);
    }


    public List<Puerto> consultarPuertos() {
        List<Puerto> listaPuertos = this.puertoDAO.consultarPuertos();
        return listaPuertos;

    }

    public boolean actualizarPuerto(Puerto puerto) {
        return this.puertoDAO.actualizarPuerto(puerto);
    }

    public boolean eliminarPuerto(String identificacion) {
        return this.puertoDAO.eliminarPuerto(identificacion);

    }
    public ArbolAVL getArbol() {
        return puertoDAO.getArbolAVL();
    }
}

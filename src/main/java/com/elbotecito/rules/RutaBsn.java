package com.elbotecito.rules;

import com.elbotecito.rules.exception.RutaYaExisteException;
import com.elbotecito.database.RutaDAO;
import com.elbotecito.database.exception.LlaveDuplicadaException;
import com.elbotecito.database.implementation.RutaDAONIO;
import com.elbotecito.model.Ruta;
import com.elbotecito.util.ArbolAVL;

import java.util.List;

public class RutaBsn {


    private RutaDAO rutaDAO;

    public RutaBsn() {
        this.rutaDAO = new RutaDAONIO();
    }

    public void guardarRuta(Ruta ruta) throws RutaYaExisteException {
        try {
            this.rutaDAO.guardarRuta(ruta);
        } catch (LlaveDuplicadaException lde) {
            throw new RutaYaExisteException();
        }
    }

    public Ruta consultarRuta(String numero) {
        return this.rutaDAO.consultarRutaPorIdentificacion(numero);
    }

    public List<Ruta> consultarRutas() {
        List<Ruta> listaRutas = this.rutaDAO.consultarRutas();
        return listaRutas;
    }

    public boolean actualizarRuta(Ruta ruta) {
        return this.rutaDAO.actualizarRuta(ruta);
    }

    public boolean eliminarRuta(String numero) {
        return this.rutaDAO.eliminarRuta(numero);
    }

    public ArbolAVL getArbol() {
        return rutaDAO.getArbolAVL();
    }

}

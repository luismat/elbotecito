package com.elbotecito.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControladorPrincipal implements Initializable {

    @FXML
    private JFXButton btnTripulacion;

    @FXML
    private JFXButton btnBarcos;

    @FXML
    private JFXButton btnRutas;

    @FXML
    private JFXButton btnReportes;

    @FXML
    private JFXButton btnInformacion;

    @FXML
    private JFXButton btnEmpresa;

    @FXML
    private TabPane Barcos;

    @FXML
    private HBox hBoxImagen;

    @FXML
    private ImageView imagenBandera;

    @FXML
    private void handleJFXButtonClicks(ActionEvent event) {

        if (event.getSource() == btnEmpresa) {
            loadStage("/view/Puerto.fxml");
        } else if (event.getSource() == btnBarcos) {
            loadStage("/view/Barco.fxml");
        } else if (event.getSource() == btnTripulacion) {
            loadStage("/view/Tripulacion.fxml");
        } else if (event.getSource() == btnRutas) {
            loadStage("/view/Ruta.fxml");
        } else if (event.getSource() == btnReportes) {
            loadStage("/view/Reporte.fxml");
        } else if (event.getSource() == btnInformacion) {
            loadStage("/view/Informacion.fxml");
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        imagenBandera.setImage(new Image("/view/images/Logo.png"));
        hBoxImagen.setAlignment(Pos.TOP_CENTER);
    }

    private void loadStage(String fxml) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxml));
            Stage stage = new Stage();
            stage.setScene(new Scene(root,1080, 720));
            stage.getIcons().add(new Image("/view/images/Logo45x45.png"));
            stage.setMinHeight(720);
            stage.setMinWidth(1080);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

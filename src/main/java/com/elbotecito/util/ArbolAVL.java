/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elbotecito.util;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @autor This code has been contributed by Mayank Jaiswal
 * https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ArbolAVL {
    private NodoAVL root;

    // A utility function to get the height of the tree
    private int height(NodoAVL N) {
        if (N == null)
            return 0;

        return N.getHeight();
    }

    // A utility function to get maximum of two integers
    int max(int a, int b) {
        return (a > b) ? a : b;
    }

    // A utility function to right rotate subtree rooted with y
    // See the diagram given above.
    private NodoAVL rightRotate(NodoAVL y) {
        NodoAVL x = y.getLeft();
        NodoAVL T2 = x.getRight();

        // Perform rotation
        x.setRight(y);
        y.setLeft(T2);

        // Update heights
        y.setHeight(max(height(y.getLeft()), height(y.getRight())) + 1);
        x.setHeight(max(height(x.getLeft()), height(x.getRight())) + 1);

        // Return new root
        return x;
    }

    // A utility function to left rotate subtree rooted with x
    // See the diagram given above.
    private NodoAVL leftRotate(NodoAVL x) {
        NodoAVL y = x.getRight();
        NodoAVL T2 = y.getLeft();

        // Perform rotation
        y.setLeft(x);
        x.setRight(T2);

        //  Update heights
        x.setHeight(max(height(x.getLeft()), height(x.getRight())) + 1);
        y.setHeight(max(height(y.getLeft()), height(y.getRight())) + 1);

        // Return new root
        return y;
    }

    // Get Balance factor of node N
    private int getBalance(NodoAVL N) {
        if (N == null)
            return 0;

        return height(N.getLeft()) - height(N.getRight());
    }

    public NodoAVL insert(NodoAVL node, int key, String Skey, long pos) {

        /* 1.  Perform the normal BST insertion */
        if (node == null)
            return (new NodoAVL(key, Skey, pos));

        if (key < node.getKey())
            node.setLeft(insert(node.getLeft(), key,Skey, pos));
        else if (key > node.getKey())
            node.setRight(insert(node.getRight(), key,Skey, pos));
        else // Duplicate keys not allowed
            return node;

        /* 2. Update height of this ancestor node */
        node.setHeight(1 + max(height(node.getLeft()),
                height(node.getRight())));

        /* 3. Get the balance factor of this ancestor
              node to check whether this node became
              unbalanced */
        int balance = getBalance(node);

        // If this node becomes unbalanced, then there
        // are 4 cases Left Left Case
        if (balance > 1 && key < node.getLeft().getKey())
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && key > node.getRight().getKey())
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && key > node.getLeft().getKey()) {
            node.setLeft(leftRotate(node.getLeft()));
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && key < node.getRight().getKey()) {
            node.setRight(rightRotate(node.getRight()));
            return leftRotate(node);
        }

        /* return the (unchanged) node pointer */
        return node;
    }

    /* Given a non-empty binary search tree, return the
        node with minimum key value found in that tree.
        Note that the entire tree does not need to be
        searched. */
    private NodoAVL minValueNodoAVL(NodoAVL node) {
        NodoAVL current = node;

        /* loop down to find the leftmost leaf */
        while (current.getLeft() != null)
            current = current.getLeft();

        return current;
    }

    public NodoAVL deleteNodoAVL(NodoAVL root, int key) {
        // STEP 1: PERFORM STANDARD BST DELETE
        if (root == null)
            return root;

        // If the key to be deleted is smaller than
        // the root's key, then it lies in left subtree
        if (key < root.getKey())
            root.setLeft(deleteNodoAVL(root.getLeft(), key));

            // If the key to be deleted is greater than the
            // root's key, then it lies in right subtree
        else if (key > root.getKey())
            root.setRight(deleteNodoAVL(root.getRight(), key));

            // if key is same as root's key, then this is the node
            // to be deleted
        else {

            // node with only one child or no child
            if ((root.getLeft() == null) || (root.getRight() == null)) {
                NodoAVL temp = null;
                if (temp == root.getLeft())
                    temp = root.getRight();
                else
                    temp = root.getLeft();

                // No child case
                if (temp == null) {
                    temp = root;
                    root = null;
                } else // One child case
                    root = temp; // Copy the contents of
                // the non-empty child
            } else {

                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                NodoAVL temp = minValueNodoAVL(root.getRight());

                // Copy the inorder successor's data to this node
                root.setKey(temp.getKey());

                // Delete the inorder successor
                root.setRight(deleteNodoAVL(root.getRight(), temp.getKey()));
            }
        }

        // If the tree had only one node then return
        if (root == null)
            return root;

        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
        root.setHeight(max(height(root.getLeft()), height(root.getRight())) + 1);

        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
        // this node became unbalanced)
        int balance = getBalance(root);

        // If this node becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(root.getLeft()) >= 0)
            return rightRotate(root);

        // Left Right Case
        if (balance > 1 && getBalance(root.getLeft()) < 0) {
            root.setLeft(leftRotate(root.getLeft()));
            return rightRotate(root);
        }

        // Right Right Case
        if (balance < -1 && getBalance(root.getRight()) <= 0)
            return leftRotate(root);

        // Right Left Case
        if (balance < -1 && getBalance(root.getRight()) > 0) {
            root.setRight(rightRotate(root.getRight()));
            return leftRotate(root);
        }

        return root;
    }

    // A utility function to print preorder traversal
    // of the tree.
    // The function also prints height of every node
 public void preOrder(NodoAVL node) {
        if (node != null) {
            System.out.print(node.getKey() + " ");
            preOrder(node.getLeft());
            preOrder(node.getRight());
        }
    }

    public NodoAVL find(int key) {
        NodoAVL aux = root;
        while (aux != null) {

            if (key == aux.getKey()) {
                return aux;
            } else if (key > aux.getKey()) {
                if (aux.getRight() != null) {
                    aux = aux.getRight();
                } else {
                    return null;
                }
            } else {
                if (aux.getLeft() != null) {
                    aux = aux.getLeft();
                } else {
                    return null;
                }
            }
        }
        return null;
    }

   public static void inOrden(NodoAVL r) {
        if (r != null) {
            inOrden(r.getLeft());
            System.out.println(r.getKey());
            inOrden(r.getRight());
        }
    }
}

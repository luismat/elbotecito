/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elbotecito.util;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author This code has been contributed by Mayank Jaiswal
 * https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 */

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NodoAVL {
    private long posicion;
    private int key;
    private String SKey;
    private int height;
    private NodoAVL left;
    private NodoAVL right;

    public NodoAVL(int d,String SKey, Long pos) {
        this.key = d;
        this.SKey = SKey;
        this.posicion = pos;
        height = 1;
    }
    public int nodosCompletos(NodoAVL t) {
        if (t==null)
            return 0;
        else {
            if (t.getLeft() != null && t.getRight() !=null)
                return  nodosCompletos(t.getLeft())+nodosCompletos(t.getRight())+1;
            return nodosCompletos(t.getLeft())+nodosCompletos(t.getRight());
        }
    }
}
  